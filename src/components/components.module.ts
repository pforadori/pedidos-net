import { NgModule } from '@angular/core';
import { ValorMesaComponent } from './valor-mesa/valor-mesa';
import { MesaResumoComponent } from './mesa-resumo/mesa-resumo';
@NgModule({
	declarations: [ValorMesaComponent,
    MesaResumoComponent],
	imports: [],
	exports: [ValorMesaComponent,
    MesaResumoComponent]
})
export class ComponentsModule {}

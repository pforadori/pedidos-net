import { Component, Input } from '@angular/core';
import { NavParams } from 'ionic-angular';

/**
 * Generated class for the MesaResumoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'mesa-resumo',
  templateUrl: 'mesa-resumo.html'
})
export class MesaResumoComponent {

  text: string;
  //@Input() itemMesa: [];
  item: any;

  constructor(public params: NavParams) {
    console.log('Hello MesaResumoComponent Component');
    this.text = 'Hello World';
  }
}

import { Component, Input, Output, OnInit } from '@angular/core';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { NavController, NavParams } from 'ionic-angular';
import { MesaPage } from '../../pages/mesa/mesa';
import { CardapioPage } from '../../pages/cardapio/cardapio';
import { MesaProvider } from '../../providers/mesa/mesa';

/**
 * Generated class for the ValorMesaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'valor-mesa',
  templateUrl: 'valor-mesa.html'
})
export class ValorMesaComponent {

  text: string;
  @Input() valor;
  //item: any;
  @Input() produtoMesa = [];

  constructor(private navCtrl: NavController, public params: NavParams, private cardapio: CardapioPage, private mesaProvider: MesaProvider) {
    console.log('Hello ValorMesaComponent Component');
  }

  mesa() {
    let itemMesa = this.mesaProvider.cartItems
    this.navCtrl.push(MesaPage, { 'item': itemMesa })
  }
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FinalizarPedidoPage } from '../finalizar-pedido/finalizar-pedido';
import { MesaProvider } from '../../providers/mesa/mesa';
import { CardapioPage } from '../cardapio/cardapio';

/**
 * Generated class for the MesaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mesa',
  templateUrl: 'mesa.html',
})
export class MesaPage {

  finalizarPedidoPage: typeof FinalizarPedidoPage;
  produto: any;
  item: any;
  itemMesa: any;
  itemMesaProduto: any;
  constructor(public navCtrl: NavController,
    public params: NavParams,
    private view: ViewController,
    private mesa: MesaProvider) {
    this.finalizarPedidoPage = FinalizarPedidoPage;
    //this.itemMesa = this.mesa.getCart()
  }

  getAll() {
    this.mesa.getCart()
      .then((result) => {
        this.itemMesa = result;
      });
  }

  ionViewDidEnter() {
    this.getAll()
  }

  closeModal() {
    this.navCtrl.setRoot(CardapioPage, { 'item': this.itemMesa })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MesaPage');
  }

  limparMesa(id) {
    this.mesa.clearCartItem(id)
    this.getAll()
  }

}

import { PerfilUsuarioPage } from './../perfil-usuario/perfil-usuario';
import { EnderecoPage } from './../endereco/endereco';
import { ContaCashbackPage } from './../conta-cashback/conta-cashback';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestaurantePage } from '../restaurante/restaurante';
import { IndiqueAmigoPage } from '../indique-amigo/indique-amigo';
import { EstabelecimentoServiceProvider } from '../../providers/estabelecimento-service/estabelecimento-service';

/**
 * Generated class for the MainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {
  tabPerfil: typeof PerfilUsuarioPage;
  tabIndiqueAmigo: typeof EnderecoPage;
  tabRestaurante: typeof RestaurantePage;
  tabCashback: typeof ContaCashbackPage;
  produtoMesa: any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private api: EstabelecimentoServiceProvider) {
    this.tabRestaurante = RestaurantePage;
    this.tabCashback = ContaCashbackPage;
    this.tabIndiqueAmigo = IndiqueAmigoPage;
    this.tabPerfil = PerfilUsuarioPage;
    this.produtoMesa = 0
    this.newMethod();
  }
  
  private newMethod() {
    this.produtoMesa = this.api.getValorMesaProduto();
  }

  mesa(){
    sessionStorage.clear()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainPage');
  }

}

import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Item } from 'ionic-angular';
import { EstabelecimentoServiceProvider } from '../../providers/estabelecimento-service/estabelecimento-service';
import { AdicionarProdutoMesaPage } from '../adicionar-produto-mesa/adicionar-produto-mesa';
import { MainPage } from '../main/main';
import { MesaProvider } from '../../providers/mesa/mesa';


/**
 * Generated class for the CardapioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-cardapio',
  templateUrl: 'cardapio.html',
})
export class CardapioPage {

  opcaoRestaurante: string;
  restaurante: any;
  listPizza: any;
  listLanche: any;
  listPromocao: any;
  listCombo: any;
  listBebida: any;
  listEsfiha: any;
  produto = '';
  comboItem: any;
  codigo: any;
  item: any;
  novoValor: any;
  valor = 0;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modal: ModalController,
    private api: EstabelecimentoServiceProvider,
    private mesa: MesaProvider) {
    this.restaurante = navParams.get('param');
    this.item = this.navParams.get('item')

    this.opcaoRestaurante = "cardapio";
    //this.list = [];
    //this.listCardapio = this.getList();


    if (this.restaurante) {
      this.getPizza(this.restaurante.Codigo);
      this.getLanche(this.restaurante.Codigo);
      this.getPromocao(this.restaurante.Codigo);
      this.getCombo(this.restaurante.Codigo);
      this.getBebida(this.restaurante.Codigo);
      this.getEsfiha(this.restaurante.Codigo);
    } else {
      this.getPizza(this.item.codigoRestaurante);
      this.getLanche(this.item.codigoRestaurante);
      this.getPromocao(this.item.codigoRestaurante);
      this.getCombo(this.item.codigoRestaurante);
      this.getBebida(this.item.codigoRestaurante);
      this.getEsfiha(this.item.codigoRestaurante);
    }

    if (this.item) {
      if (this.item.valorMesa > 0) {
        this.valor = this.item.valorMesa + (this.item.valor * this.item.quantidade)
      } else {
        this.valor = this.item.valor * this.item.quantidade
      }
    }

    this.montarMesa()
  }

  popAll() {
    this.navCtrl.setRoot(MainPage)
    this.mesa.clearCart()
  }

  getPromocao(id) {
    this.api.promocaoEstabelecimentoById(id)
      .then((result: any) => {
        this.listPromocao = result;
      })
      .catch((error: any) => {
        //this.toast.create({ message: 'Erro ao recuperar o usuário. Erro: ' + error.error, position: 'botton', duration: 3000 }).present();
      });
  }

  getCombo(id) {
    this.api.comboEstabelecimentoById(id)
      .then((result: any) => {
        this.listCombo = result;
      })
      .catch((error: any) => {
        //this.toast.create({ message: 'Erro ao recuperar o usuário. Erro: ' + error.error, position: 'botton', duration: 3000 }).present();
      });
  }

  getPizza(id) {
    this.api.pizzaEstabelecimentoById(id)
      .then((result: any) => {
        this.listPizza = result;
      })
      .catch((error: any) => {
        //this.toast.create({ message: 'Erro ao recuperar o usuário. Erro: ' + error.error, position: 'botton', duration: 3000 }).present();
      });
  }

  getLanche(id) {
    this.api.lancheEstabelecimentoById(id)
      .then((result: any) => {
        this.listLanche = result;
      })
      .catch((error: any) => {
        //this.toast.create({ message: 'Erro ao recuperar o usuário. Erro: ' + error.error, position: 'botton', duration: 3000 }).present();
      });
  }

  getEsfiha(id) {
    this.api.esfihaEstabelecimentoById(id)
      .then((result: any) => {
        this.listEsfiha = result;
      })
      .catch((error: any) => {
        //this.toast.create({ message: 'Erro ao recuperar o usuário. Erro: ' + error.error, position: 'botton', duration: 3000 }).present();
      });
  }

  getBebida(id) {
    this.api.bebidaEstabelecimentoById(id)
      .then((result: any) => {
        this.listBebida = result;
      })
      .catch((error: any) => {
        //this.toast.create({ message: 'Erro ao recuperar o usuário. Erro: ' + error.error, position: 'botton', duration: 3000 }).present();
      });
  }

  openModal(item, cozinha) {
    if (cozinha == 'esfiha') {
      item.ValorTradicional = item.Valor
    } else if (cozinha == 'bebida') {
      item.Descricao = item.Nome
      item.Ingredientes = item.Tamanho
      item.ValorTradicional = item.Valor
    } else if (cozinha == 'promocao') {
      item.Descricao = item.NomeProduto
      //item.Ingredientes = item.Descricao
      item.ValorTradicional = item.ValorPromocional
    } else if (cozinha == 'combo') {
      item.Descricao = item.Nome
      item.ValorTradicional = item.Valor
    }

    item.ValorMesa = this.valor

    let myModal = this.modal.create(AdicionarProdutoMesaPage, { 'produto': item });
    myModal.present();
  }

  montarMesa() {
    //let produtoMesa = this.navParams.get('item')
    let produtoMesa = this.mesa.getCart()
    this.mesa.getCart()
    .then((result) => {
      produtoMesa = result;
    });
    if (produtoMesa)
      //this.addToCart(produtoMesa.item, produtoMesa.quantidade, produtoMesa.valor, produtoMesa.valorMesa)
      this.addToCart(produtoMesa)
  }

  //addToCart(item, quantidade, valor, valorMesa) {
    addToCart(produto) {
    let itemMesa = new ItemMesa();
    itemMesa.item = produto.item;
    itemMesa.quantidade = produto.quantidade;
    itemMesa.valor = produto.valor;
    itemMesa.valorMesa = produto.valorMesa;
    //this.mesa.addToCart(itemMesa);
  }
}

export class ItemMesa {
  //id: number;
  item: string;
  quantidade: number;
  valor: number;
  valorMesa: number;
  codigoRestaurante: number;
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { EditUserPage } from '../edit-user/edit-user';
import { MyLocalPage } from '../my-local/my-local';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the PerfilUsuarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-perfil-usuario',
  templateUrl: 'perfil-usuario.html',
})
export class PerfilUsuarioPage {

  editUserPage: any;
  item: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {
    //this.editUserPage = EditUserPage;
  }

  selectEditUser() {
    this.navCtrl.push(EditUserPage);
  }
  selectLocal(){
    this.navCtrl.push(MyLocalPage);
  }

  exit() {
    let alert = this.alertCtrl.create({
      title: 'Sair do app',
      message: 'Deseja realmente sair do aplicativo?',
      buttons: [
        {
          text: 'Nao',
          role: 'cancel',
          handler: () => {}
        },
        {
          text: 'Sim',
          handler: () => {
            localStorage.removeItem('dadosUsuario')
            window.location.reload(); 
          }
        }
      ]
    });
    alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilUsuarioPage');
  }

}

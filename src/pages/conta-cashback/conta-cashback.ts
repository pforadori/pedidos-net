import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';

/**
 * Generated class for the ContaCashbackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-conta-cashback',
  templateUrl: 'conta-cashback.html',
})
export class ContaCashbackPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private contacts: Contacts) {
  }

  openList(){
    //let contact: Contact = this.contacts.find(name);
    console.log(this.contacts.find(name));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContaCashbackPage');
  }

}

// contact.name = new ContactName(null, 'Smith', 'John');
// contact.phoneNumbers = [new ContactField('mobile', '6471234567')];
// contact.save().then(
//   () => console.log('Contact saved!', contact),
//   (error: any) => console.error('Error saving contact.', error)
// );

//find(fields, options)
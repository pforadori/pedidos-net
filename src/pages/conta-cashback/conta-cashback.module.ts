import { NgModule } from '@angular/core';
import { IonicPageModule, NavController, ViewController } from 'ionic-angular';
import { ContaCashbackPage } from './conta-cashback';


@NgModule({
  declarations: [
    ContaCashbackPage,
  ],
  imports: [
    IonicPageModule.forChild(ContaCashbackPage),
  ],
})
export class ContaCashbackPageModule {

  
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IndiqueAmigoPage } from './indique-amigo';

@NgModule({
  declarations: [
    IndiqueAmigoPage,
  ],
  imports: [
    IonicPageModule.forChild(IndiqueAmigoPage),
  ],
})
export class IndiqueAmigoPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CadastrarUsuarioPage } from '../cadastrar-usuario/cadastrar-usuario';
import { EsqueciSenhaPage } from '../esqueci-senha/esqueci-senha';
import { MainPage } from '../main/main';
import { Facebook } from '../../../node_modules/@ionic-native/facebook';
import { LoginServiceProvider } from '../../providers/login-service/login-service';
import { FormBuilder, Validators } from '@angular/forms';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  cadastrarUsuarioPage: any;
  esqueciSenhaPage: any;
  mainPage: any;
  salvarService: any;
  public autenticarForm: any;
  autenticarModel: Autenticar;

  errorNumeroCelular: boolean;
  messageNumeroCelular: string;
  errorSenha: boolean;
  messageSenha: string;
  messageCelular: string;
  errorCelular: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, private facebook: Facebook, private api: LoginServiceProvider, public formBuilder: FormBuilder) {
    this.cadastrarUsuarioPage = CadastrarUsuarioPage;
    this.esqueciSenhaPage = EsqueciSenhaPage;
    this.mainPage = MainPage;

    this.autenticarModel = new Autenticar()
    this.autenticarForm = formBuilder.group({
      celular: ['', Validators.required],
      senha: ['', Validators.required],
    });
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  loginFacebook() {
    console.log("login com facebook");
    let permissions = new Array<string>();
    permissions = ["public_profile", "email"];

    this.facebook.login(permissions).then((response) => {
      let params = new Array<string>();

      this.facebook.api("/me?fields=name,email", params)
        .then(res => {

          //estou usando o model para criar os usuarios
          let usuario = new Usuario();
          usuario.nome = res.name;
          usuario.email = res.email;
          usuario.senha = res.id;
          usuario.login = res.email;

          this.logar(usuario);
          
        }, (error) => {
          alert(error);
          console.log('ERRO LOGIN: ', error);
        })
    }, (error) => {
      alert(error);
    });
  }

  logar(usuario: Usuario) {
    this.facebook.getLoginStatus()
    //this.salvarService.salvarFacebook(usuario)
      .then(() => {
        this.navCtrl.push(MainPage);
        console.log('Usuario cadastrado via facebook com sucesso!');
      })
  }

  autenticar(){
    let { senha, celular} = this.autenticarForm.controls;

    if (!this.autenticarForm.valid) {
      if (!celular.valid || !senha.valid) {
        this.errorCelular = true;
        this.messageCelular = "Ops! Celular ou senha inválidos!";
      } else {
        this.messageCelular = "";
      }

    } else {
    let objeto = this.autenticarForm.getRawValue();
    
    objeto.celular =  celular.value.replace("-","").replace("(","").replace(")","").replace(" ","")
    this.api.login(objeto)
      .then((result: any) => {
        console.log('Usuário autenticado com sucesso. Token: ' + result);
        if (result == null || result == ""){
          this.errorCelular = true
          this.messageCelular = "Ops! Celular ou senha inválidos!";
        }else{
          this.navCtrl.push(MainPage)
        }
      })
      .catch((error: any) => {
        console.log('Erro ao autenticar usuário. Erro: ' + error);
        this.errorCelular = true;
        this.messageCelular = error;
      });
      
    }
  }
}

export class Model {
  constructor(objeto?) {
    Object.assign(this, objeto);
  }
}

export class Usuario extends Model {
  codigo: number;
  nome: string;
  email: string;
  login: string;
  senha: string;
}

export class Autenticar {
  numeroCelular: number;
  senha: string;
}
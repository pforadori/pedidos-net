import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the RegisterLocalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-local',
  templateUrl: 'register-local.html',
})
export class RegisterLocalPage {

  cep: string[] = [];
  outro: string[] = ["0"];
  constructor(private view: ViewController) {
  }


  closeModal(){
    this.view.dismiss();
  }

  verifica(){
    console.log("ok");
    this.cep = ["0"];
    this.outro = [];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterLocalPage');
  }

}

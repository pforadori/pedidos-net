import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterLocalPage } from './register-local';

@NgModule({
  declarations: [
    RegisterLocalPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterLocalPage),
  ],
})
export class RegisterLocalPageModule {}

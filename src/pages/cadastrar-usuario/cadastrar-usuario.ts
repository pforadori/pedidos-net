import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';
import { MainPage } from '../main/main';
import { FormBuilder, Validators, AbstractControl, FormGroup } from '@angular/forms';
import { Facebook } from '@ionic-native/facebook';
import { LoginServiceProvider } from '../../providers/login-service/login-service';
import { EditUserPage } from '../edit-user/edit-user';

/**
 * Generated class for the CadastrarUsuarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cadastrar-usuario',
  templateUrl: 'cadastrar-usuario.html',
})
export class CadastrarUsuarioPage {
  mainPage: any;
  public loginForm: any;
  messageEmail = ""
  messagePassword = "";
  messageNome = "";
  messageCpf = "";
  messageCelular = "";
  messageTermo = "";
  messageGeral = "";

  errorEmail = false;
  errorPassword = false;
  errorNome = false;
  errorCpf = false;
  errorCelular = false;
  errorTermo = false;
  errorGeral = false;
  salvarService: any;

  model: User;

  constructor(public navCtrl: NavController, 
    private modal: ModalController, 
    public formBuilder: FormBuilder, 
    private facebook: Facebook, 
    private api: LoginServiceProvider, 
    private toast: ToastController) {
    this.model = new User();
    this.loginForm = formBuilder.group({
      //email: ['', Validators.required],
      nome: ['', Validators.required],
      senha: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(20),
      Validators.required])],
      numeroCelular: ['', Validators.required],
      cpf: ['', Validators.required],
      termo: [undefined, Validators.required],
    });

  }

  login() {
    let { nome, senha, numeroCelular, cpf,  termo } = this.loginForm.controls;

    if (!this.loginForm.valid) {
      // if (!email.valid) {
      //   this.errorEmail = true;
      //   this.messageEmail = "Ops! Email inválido";
      // } else {
      //   this.messageEmail = "";
      // }
      if (!numeroCelular.valid) {
        this.errorCelular = true;
        this.messageCelular = "Ops! Digite seu celular";
      } else {
        this.messageCelular = "";
      }

      if (!nome.valid) {
        this.errorNome = true;
        this.messageNome = "Ops! Digite seu nome";
      } else {
        this.messageNome = "";
      }

      if (!cpf.valid) {
        this.errorCpf = true;
        this.messageCpf = "Ops! Digite seu nome";
      } else {
        this.messageCpf = "";
      }

      if (!senha.valid) {
        this.errorPassword = true;
        this.messagePassword = "A senha precisa ter de 6 a 20 caracteres"
      } else {
        this.messagePassword = "";
      }

      if (termo.valid) {
        this.messageTermo = "";
      } else {
        this.errorTermo = true;
        this.messageTermo = "Voce deve concordar com os termos antes";
      }
    }
    else {
      let objeto:any// = this.loginForm.getRawValue();

      // {
      //   "Nome": "Nathana Reboucas",
      //   "senha": "teste123",
      //   "NumeroCelular": "11971720212",
      //   "cpf": "37298596845",
      //   "facebookAccessToken": ""
      // }

      //Nome: "Pedro Foradori", senha: "teste123", NumeroCelular: "11981024516", cpf: "416220.24869"

      objeto = {
        Nome: nome.value,
        senha: senha.value,
        NumeroCelular: numeroCelular.value.replace("-","").replace("(","").replace(")","").replace(" ",""),
        cpf: cpf.value.replace(".","").replace("-","")
      }

      let objetoJson = JSON.stringify(objeto)
      debugger
      //objeto.numeroCelular = numeroCelular.value.replace("-","").replace("(","").replace(")","").replace(" ","")
      //objeto.cpf = cpf.value.replace(".","").replace("-","")

      this.api.createAccount(objetoJson)
        .then((result: any) => {
          //this.toast.create({ message: 'Usuário criado com sucesso. Token: ' + result.token, 
          //position: 'botton', duration: 3000 }).present();
          console.log('Usuário criado com sucesso. Token: ' + result);  

          let objetoLogin:any
          objetoLogin = { celular: objeto.NumeroCelular, senha: objeto.senha };
          this.api.login(objetoLogin)

          this.navCtrl.push(MainPage)
        })
        .catch((error: any) => {
          //this.toast.create({ message: 'Erro ao criar o usuário. Erro: ' + error.error, 
          //position: 'botton', duration: 3000 }).present();
          this.errorGeral = true
          this.messageGeral = error
          console.log('Erro ao criar o usuário. Erro: ' + error);
        });
    }
  }
  
  openModal() {
    const myModal = this.modal.create('TermoUsoPage');
    myModal.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastrarUsuarioPage');
  }
}

export class User {
  //email: string;
  celular: number;
  nome: string;
  cpf: number;
  password: string;
}

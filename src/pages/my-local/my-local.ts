import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

/**
 * Generated class for the MyLocalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-local',
  templateUrl: 'my-local.html',
})
export class MyLocalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private modal: ModalController) {
    //this.mainPage = MainPage;
  }

  openModal() {
    const myModal = this.modal.create('RegisterLocalPage');
    myModal.present();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad MyLocalPage');
  }

}

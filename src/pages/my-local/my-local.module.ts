import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyLocalPage } from './my-local';

@NgModule({
  declarations: [
    MyLocalPage,
  ],
  imports: [
    IonicPageModule.forChild(MyLocalPage),
  ],
})
export class MyLocalPageModule {}

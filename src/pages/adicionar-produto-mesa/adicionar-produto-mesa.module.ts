import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarProdutoMesaPage } from './adicionar-produto-mesa';

@NgModule({
  declarations: [
    AdicionarProdutoMesaPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarProdutoMesaPage),
  ],
})
export class AdicionarProdutoMesaPageModule {}

import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, ViewController, ModalController, NavParams } from 'ionic-angular';
import { EstabelecimentoServiceProvider } from '../../providers/estabelecimento-service/estabelecimento-service';
import { MesaPage } from '../mesa/mesa';
import { CardapioPage } from '../cardapio/cardapio';
import { MesaProvider } from '../../providers/mesa/mesa';

/*
 * Generated class for the AdicionarProdutoMesaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-adicionar-produto-mesa',
  templateUrl: 'adicionar-produto-mesa.html',
})
export class AdicionarProdutoMesaPage {

  listEstabelecimento: any;
  quantidade: any;
  produtoMesa: any;
  produto: any;
  comboItem: any;
  objetoItem: {};
  valor: { valor: any; };
  id: any;
  arrayItem = new Array()
  itemMesa = [];

  constructor(public navCtrl: NavController,
    private view: ViewController,
    private modal: ModalController,
    params: NavParams,
    private api: EstabelecimentoServiceProvider,
    private mesa: MesaProvider) {
    this.quantidade = 1;
    this.produtoMesa;
    this.produto = params.get('produto');
    this.comboItem = this.produto.ComboItems;
    this.produto.Codigo = this.produto.CodigoEstabelecimento
  }

  maisUm() {
    if (this.quantidade == null) {
      this.quantidade = 1;
    } else {
      this.quantidade++;
    }
  }

  menosUm() {
    if (this.quantidade == null) {
      return
    }
    else if (this.quantidade <= 1) {
      return
    } else {
      this.quantidade--;
    }
  }

  closeModal() {
    this.view.dismiss();
  }

  adicionarProdutoMesa() {
    console.log("produto adicionado a mesa");
    this.produtoMesa = this.quantidade;

    this.objetoItem = {
      item: this.produto.Descricao,
      quantidade: this.quantidade,
      valor: this.produto.ValorTradicional,
      codigoRestaurante: this.produto.Codigo,
      valorMesa: this.produto.ValorMesa
    }

    this.navCtrl.setRoot(CardapioPage, { 'item': this.objetoItem });
    this.mesa.addToCart(this.objetoItem)
    //this.navCtrl.goToRoot()
    //this.closeModal()
  }

  openModal() {
    //if (localStorage.getItem('objetoItem') != null) {
    let myModal = this.modal.create(MesaPage, { 'item': this.objetoItem });
    myModal.present();
    //}
  }

  estabelecimento(id) {
    this.api.buscarEstabelecimentoById(id)
      .then((result: any) => {
        this.navCtrl.push(CardapioPage, { param: result })
      })
      .catch((error: any) => { });
  }

  // itemSelected(listEstabelecimento: string) {
  //   console.log("Selected Item", listEstabelecimento);
  //   this.navCtrl.push(MesaPage, {
  //     param: listEstabelecimento,
  //   });
  // }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdicionarProdutoMesaPage');
  }
}

import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';

/**
 * Generated class for the TermoUsoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-termo-uso',
  templateUrl: 'termo-uso.html',
})
export class TermoUsoPage {

  constructor(private view: ViewController) {
  }

  closeModal(){
      this.view.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TermoUsoPage');
  }

}

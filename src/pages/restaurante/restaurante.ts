import { CardapioPage } from './../cardapio/cardapio';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { EstabelecimentoServiceProvider } from '../../providers/estabelecimento-service/estabelecimento-service';
import { Geolocation } from '@ionic-native/geolocation'

/**
 * Generated class for the RestaurantePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-restaurante',
  templateUrl: 'restaurante.html',
})
export class RestaurantePage {
  saldoNome: any[];
  cardapioPage: typeof CardapioPage;
  listEstabelecimento: any;
  list: any[];
  restaurante: any;
  codigoEstabelecimento: any;
  //item: any[];
  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private api: EstabelecimentoServiceProvider, 
              private geolocation: Geolocation, 
              private platform: Platform,
              private view: ViewController) {
    //this.item = [];
    //this.listEstabelecimento = []
    //this.codigoEstabelecimento = sessionStorage.getItem('codigoEstabelecimento')
    //if(this.codigoEstabelecimento != null || isNaN(this.codigoEstabelecimento)){
      //this.itemSelected(this.codigoEstabelecimento)
      //sessionStorage.clear()
   // }
    
    //this.cardapioPage = CardapioPage;
    this.saldoNome = this.getSaldoNome();
    
    this.platform.ready().then(() => {
      this.geolocation.getCurrentPosition().then(res => {
        this.getListEstabelecimento(res.coords.latitude, res.coords.longitude);
        }).catch(() => {
        console.log("erro ao pegar geolocalizacao ");
        })
      })

      //teste no emulador do iphone sem localicazao
      //this.getListEstabelecimento(-24.1300811767578, -46.68701171875);
  }

  // getListEstabelecimento(latitude, longitude){
  //   this.list = [
  //     {cashBack: "10%", nomeEstabelecimento: "Nome estabelecimento 1", tipo: "Pizza", endereco: "Endereco Fake 1"},
  //    ]
  //   return this.list;
  // }

  getListEstabelecimento(latitude: any, longitude: any) {
    this.api.buscarEstabelicimento(latitude, longitude)
      .then((result: any) => {
        this.listEstabelecimento = result;
      })
      .catch((error: any) => {
        //this.toast.create({ message: 'Erro ao recuperar o usuário. Erro: ' + error.error, position: 'botton', duration: 3000 }).present();
      });
  }

  getSaldoNome(){
    this.list = [
      {saldo: "40,00", nomeUsuario: "Fulano"},
     ]
    return this.list;
  }

  itemSelected(id) {
    this.api.buscarEstabelecimentoById(id)
    .then((result: any) => {
      this.navCtrl.push(CardapioPage, { param: result  })
      //this.view.dismiss()
    })
    .catch((error: any) => {});
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad RestaurantePage');
  }

}

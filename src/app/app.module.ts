import { PerfilUsuarioPage } from './../pages/perfil-usuario/perfil-usuario';
import { EnderecoPage } from './../pages/endereco/endereco';
import { ContaCashbackPage } from './../pages/conta-cashback/conta-cashback';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { ApresentacaoPage } from '../pages/apresentacao/apresentacao';
import { LoginPage } from '../pages/login/login';
import { CadastrarUsuarioPage } from '../pages/cadastrar-usuario/cadastrar-usuario';

import { StatusBar } from '@ionic-native/status-bar';
import { EsqueciSenhaPage } from '../pages/esqueci-senha/esqueci-senha';
import { MainPage } from '../pages/main/main';
import { CardapioPage } from '../pages/cardapio/cardapio';
import { RestaurantePage } from '../pages/restaurante/restaurante';
import { IndiqueAmigoPage } from '../pages/indique-amigo/indique-amigo';
import { FinalizarPedidoPage } from '../pages/finalizar-pedido/finalizar-pedido';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';
import { EditUserPage } from '../pages/edit-user/edit-user';
import { MyLocalPage } from '../pages/my-local/my-local';
import { Facebook } from '@ionic-native/facebook';
import { HttpModule } from '@angular/http';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { LoginServiceProvider } from '../providers/login-service/login-service';
import { EstabelecimentoServiceProvider } from '../providers/estabelecimento-service/estabelecimento-service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Geolocation  } from '@ionic-native/geolocation';
import { AdicionarProdutoMesaPage } from '../pages/adicionar-produto-mesa/adicionar-produto-mesa';
import { MesaPage } from '../pages/mesa/mesa';
import { ValorMesaComponent } from '../components/valor-mesa/valor-mesa';
import { MesaResumoComponent } from '../components/mesa-resumo/mesa-resumo';

import { MesaProvider } from '../providers/mesa/mesa';
import { IonicStorageModule } from '@ionic/storage'

@NgModule({
  declarations: [
    MyApp,
    ApresentacaoPage,
    LoginPage,
    CadastrarUsuarioPage,
    EsqueciSenhaPage,
    MainPage,
    CardapioPage,
    RestaurantePage,
    ContaCashbackPage,
    EnderecoPage,
    PerfilUsuarioPage,
    IndiqueAmigoPage,
    FinalizarPedidoPage,
    EditUserPage,
    MyLocalPage,
    AdicionarProdutoMesaPage,
    MesaPage,
    ValorMesaComponent,
    MesaResumoComponent,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),    
    [HttpModule],
    BrMaskerModule,
    HttpClientModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ApresentacaoPage,
    LoginPage,
    CadastrarUsuarioPage,
    EsqueciSenhaPage,
    MainPage,
    CardapioPage,
    RestaurantePage,
    ContaCashbackPage,
    EnderecoPage,
    PerfilUsuarioPage,
    IndiqueAmigoPage,
    FinalizarPedidoPage,
    EditUserPage,
    MyLocalPage,
    AdicionarProdutoMesaPage,
    MesaPage,
    ValorMesaComponent,
    MesaResumoComponent
  ],
  providers: [
    Contacts,
    StatusBar,
    Facebook,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ScreenOrientation,
    LoginServiceProvider,
    EstabelecimentoServiceProvider,
    Geolocation,
    MesaProvider,
  ]
})
export class AppModule {}


import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { ApresentacaoPage } from '../pages/apresentacao/apresentacao';
import { MainPage } from '../pages/main/main';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  
  rootPage: any = this.paginaInicial();

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform) {
    

    // used for an example of ngFor and navigation
    this.pages = [];

    // dbProvider.createDb()
    // .then(() => {
    //   this.initializeApp();
    // })
    // .catch(() => {
    //   this.initializeApp();
    // })
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      //this.statusBar.styleDefault();
      //this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  paginaInicial(){
    let usuarioLogado = localStorage.getItem('dadosUsuario');
    let pageInicial: any
    if (usuarioLogado == null) {
      pageInicial = ApresentacaoPage
    }else{
      pageInicial = MainPage
    }

    return pageInicial
  }

}

import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Content } from 'ionic-angular';
import { viewClassName } from '@angular/compiler';

@Injectable()
export class LoginServiceProvider {
  private apiUrl = 'https://hapi.pedidosnet.com/api/v10/'

  constructor(public http: Http) {
    console.log('Hello PedidosServiceProvider Provider');
    
  }

  createAccount(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl + 'cliente/Cadastrar', JSON.parse(data))
        .subscribe((result: any) => {
          resolve(result.json());
        },
          (error) => {
            reject(error.json());
            console.log(error)
          });
    });
  }

  login(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl + 'cliente/autenticar', data)
        .subscribe((result: any) => {
          resolve(result.json());
          localStorage.setItem('dadosUsuario', result._body)
        },
        (error) => {
          reject(error.json());
        });
    });
  }


}

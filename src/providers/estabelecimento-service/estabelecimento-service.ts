import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { resolveDefinition } from '@angular/core/src/view/util';

/*
  Generated class for the EstabelecimentoServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EstabelecimentoServiceProvider {
  valorAnterior: any

  constructor(public http: HttpClient) {
    console.log('Hello EstabelecimentoServiceProvider Provider');
  }

  private apiUrl = 'http://hapi.pedidosnet.com/api/v10/'

  buscarEstabelicimento(latitude, longitude) {
    return new Promise((resolve, reject) => {
      let url = this.apiUrl + 'estabelecimento/ObterEstabelecimentoPorLatLong?latitude=' + latitude + '&longitude=' + longitude;
      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result.data);
        },
          (error) => {
            reject(error.json());
          });
    });
  }

  buscarEstabelecimentoById(id) {
    return new Promise((resolve, reject) => {
      let url = this.apiUrl + 'estabelecimento/ObterEstabelecimentosPorId?codigoEstabelecimento=' + id;
      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result.data);
        },
          (error) => {
            reject(error.json());
          });
    });
  }

  promocaoEstabelecimentoById(id) {
    return new Promise((resolve, reject) => {
      let url = this.apiUrl + 'estabelecimento/ObterPromocoesPorEstabelecimento?codigoEstabelecimento=' + id;

      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result.data);
        },
          (error) => {
            reject(error.json());
          });
    });
  }

  pizzaEstabelecimentoById(id) {
    return new Promise((resolve, reject) => {
      let url = this.apiUrl + 'estabelecimento/ObterPizzasPorEstabelecimento?codigoEstabelecimento=' + id;

      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result.data);
        },
          (error) => {
            reject(error.json());
          });
    });
  }

  lancheEstabelecimentoById(id) {
    return new Promise((resolve, reject) => {
      let url = this.apiUrl + 'estabelecimento/ObterLanchesPorEstabelecimento?codigoEstabelecimento=' + id;

      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result.data);
        },
          (error) => {
            reject(error.json());
          });
    });
  }

  comboEstabelecimentoById(id) {
    return new Promise((resolve, reject) => {
      let url = this.apiUrl + 'estabelecimento/ObterCombosPorEstabelecimento?codigoEstabelecimento=' + id;

      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result.data);
        },
          (error) => {
            reject(error.json());
          });
    });
  }

  esfihaEstabelecimentoById(id) {
    return new Promise((resolve, reject) => {
      let url = this.apiUrl + 'estabelecimento/ObterEsfihasPorEstabelecimento?codigoEstabelecimento=' + id;

      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result.data);
        },
          (error) => {
            reject(error.json());
          });
    });
  }

  bebidaEstabelecimentoById(id) {
    return new Promise((resolve, reject) => {
      let url = this.apiUrl + 'estabelecimento/ObterBebidasPorEstabelecimento?codigoEstabelecimento=' + id;

      this.http.get(url)
        .subscribe((result: any) => {
          resolve(result.data);
        },
          (error) => {
            reject(error.json());
          });
    });
  }

  setValorMesaProduto(valor) {
    //this.valorAnterior = sessionStorage.getItem('valorMesa')
    valor += valor
    //sessionStorage.setItem('valorMesa', valor)
    return valor
  }

  getValorMesaProduto() {
    return parseFloat(sessionStorage.getItem('valorMesa'))
  }
} 
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Item } from 'ionic-angular';

/*
  Generated class for the MesaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MesaProvider {

  cartItems: any[];
  mesa: any[];
  itemMesa: any;

  constructor(private storage: Storage) {
    this.storage.get('cart')
      .then((value) => {
        if (value == null) {
          this.cartItems = [];
        } else {
          this.cartItems = value;
        }
      });
  }

  getCart(): Promise<any> {
    return this.storage.get('cart');
  }

  addToCart(item: any): Promise<any> {
    this.cartItems.push(item)
    return this.storage.set('cart', this.cartItems);
  }

  addToCartArray(item: any): Promise<any> {
    return this.storage.set('cart', item);
  }

  clearCart(): Promise<any> {
    this.cartItems = [];
    return this.storage.remove('cart');
  }

  clearCartItem(item: any): Promise<any> {
    this.mesa = []
    this.cartItems.splice(item, 1)
    this.mesa = this.cartItems
    this.clearCart()
    return this.addToCartArray(this.mesa)
  }

}
